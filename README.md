# XTerm.js UOSec
This is a game for new members to play to teach basic terminal skills. It is 100% browser based and mobile-friendly to minimize hosting costs and maximize accessibility.

## Development

After cloning the repository, use npm to install dependencies:

```shell
$ npm install
```

Then, use `npm run serve`, to build the project and host a local webserver on port 8080.

The source code is in `src/index.js`, and the html and css are in `static/index.html`, and `static/main.css`, respectively.

## Current issues and todos:

- [ ] Command history is broken when the cursor is not at the end of the line
- [ ] Add commands to move around and interact with the virtual filesystem (cd, ls, mkdir, touch, cat)
- [ ] Add useful help text to the page opening (basics on how to get help and what the goal of the challenge is)
- [ ] Make the site prettier and hackery
- [ ] Make the terminal colorscheme hackery
- [ ] Test on mildy techy friends