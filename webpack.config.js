const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");


module.exports = {
  mode: 'development',
  devtool: 'source-map',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    fallback: {
        assert: require.resolve("assert"),
        fs: false,
        path: require.resolve("path-browserify")
    },
    alias: {
        process: "process/browser",
    }
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "node_modules/xterm/css/xterm.css", to: "xterm.css" },
        { from: "static/index.html", to: "index.html" },
        { from: "static/main.css", to: "main.css" },
        { from: "static/pexels-panumas-nikhomkhai-1148820.webp", to: "background.webp" },
      ],
    }),
  ],
    module: {
    rules: [
      {
        test: /\.txt$/i,
        use: [
          {
            loader: 'raw-loader',
            options: {
              esModule: false,
            },
          },
        ],
      },
    ],
  },
};