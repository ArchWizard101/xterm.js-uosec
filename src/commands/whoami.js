const arg = require('arg');
import helpMessage from 'raw-loader!../manpages/whoami';

export default function su (argv, env) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return helpMessage
        }

        return env["curr_username"]
    } catch (err) {
	if (err.code === 'ARG_UNKNOWN_OPTION') {
		return err.message
	} else {
		throw err;
	}}
}