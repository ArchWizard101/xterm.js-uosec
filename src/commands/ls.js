const arg = require('arg');
import helpMessage from 'raw-loader!../manpages/ls';

export default function ls (argv, env, filesystem) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return helpMessage
        }
        // Determine the path to the directory to list
        let path = ""

        if(args['_'].length > 1) {
            return "Error: too many arguments, use `ls --help` for help"
        }

        if(args['_'].length != 1) {
            path = env["curr_directory"]
        } else {
            if (args['_'][0].startsWith("/")) {
                path = args['_'][0]
            } else {
                path = `${env["curr_directory"]}/${args['_'][0]}`.replace("//","/")
            }
        }

        if (path.endsWith("/")) {
            path = path.slice(0,-1)
        }

        console.log(path)

        // traverse the filesystem to get the appropriate directory
        path = path.split("/").slice(1)

        if (path[0] == [""]) {
            path = [];
        } 

        console.log(path)

        let directory = filesystem
        for (var i = 0; i < path.length; i++) {
            console.log(i)
            let found = false;
            // Check for the two special cases
            if (path[i] === ".."){
                if (directory.name === "/") {
                    found = true;
                } else {
                    directory = directory.parentDir
                    found = true;
                }
            }
            else if (path[i] === ".") {
                directory = directory;
                found = true;
            }
            else {
                for (var j = 0; j < directory.subdirectories.length; j++) {
                    console.log(directory.subdirectories, j)
                    if (directory.subdirectories[j].name === path[i]) {
                        found = true
                        directory = directory.subdirectories[j]
                        break;
                    }
                }
                for (var j = 0; j < directory.files.length; j++) {
                    if (directory.files[j].name === path[i]) {
                        return "Error, not a directory!"
                    }
                }
            }
            if (!found) {
                return "Error: No such directory"
            }
        }

        var names = [];
        for (var i = 0; i < directory.subdirectories.length; i++) {
            names.push(directory.subdirectories[i].name);
        }
        for (var i = 0; i < directory.files.length; i++) {
            names.push(directory.files[i].name);
        }

        return names.join(' ')

    } catch (err) {
	if (err.code === 'ARG_UNKNOWN_OPTION') {
		return err.message
	} else {
		throw err;
	}}
}