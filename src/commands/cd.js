const arg = require('arg');
import helpMessage from 'raw-loader!../manpages/cd';

export default function cd (argv, env, filesystem) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return helpMessage
        }
        // Determine the path to the directory to change to
        let path = ""

        if(args['_'].length != 1) {
            return "Error: No directory specified. Use `cd --help` for help"

        } else {
            if (args['_'][0].startsWith("/")) {
                path = args['_'][0]
            } else {
                path = `${env["curr_directory"]}/${args['_'][0]}`.replace("//","/")
            }
        }

        if (path.endsWith("/")) {
            path = path.slice(0,-1)
        }

        console.log(path)

        // traverse the filesystem to check that the appropriate directory exists
        let list_path = path.split("/").slice(1)

        if (list_path[0] == [""]) {
            list_path = [];
        } 

        let directory = filesystem
        for (var i = 0; i < list_path.length; i++) {
            console.log(i)
            let found = false;
            // Check for the two special cases
            if (list_path[i] === ".."){
                if (directory.name === "/") {
                    found = true;
                } else {
                    directory = directory.parentDir
                    found = true;
                }
            }
            else if (list_path[i] === ".") {
                directory = directory;
                found = true;
            }
            else {
                for (var j = 0; j < directory.subdirectories.length; j++) {
                    console.log(directory.subdirectories, j)
                    if (directory.subdirectories[j].name === list_path[i]) {
                        found = true
                        directory = directory.subdirectories[j]
                        break;
                    }
                }
                for (var j = 0; j < directory.files.length; j++) {
                    if (directory.files[j].name === list_path[i]) {
                        return "Error, not a directory!"
                    }
                }
            }
            if (!found) {
                return "Error: No such directory"
            }
        }

        // Walk back up the tree to find the final path
        let final_path = []
        while (directory.name != "/") {
            final_path.push(directory.name)
            directory = directory.parentDir
        }

        final_path = final_path.reverse().join("/")

        console.log(final_path)
        env["curr_directory"] = `/${final_path}`

        return

    } catch (err) {
	if (err.code === 'ARG_UNKNOWN_OPTION') {
		return err.message
	} else {
		throw err;
	}}
}