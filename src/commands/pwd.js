const arg = require('arg');
import pwdHelpMessage from 'raw-loader!../manpages/pwd';

export default function pwd (argv, env) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return pwdHelpMessage
        }

        return env["curr_directory"]

    } catch (err) {
	if (err.code === 'ARG_UNKNOWN_OPTION') {
		return err.message
	} else {
		throw err;
	}}
}
