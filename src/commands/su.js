const arg = require('arg');
import suHelpMessage from 'raw-loader!../manpages/su';

export default function su (argv, env) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return suHelpMessage
        }

        if(args['_'].length != 1) {
            return "Error: no user specified. Use `su --help` for help"
        }

        env["curr_username"] = args['_'][0]
    } catch (err) {
	if (err.code === 'ARG_UNKNOWN_OPTION') {
		return err.message
	} else {
		throw err;
	}}
}