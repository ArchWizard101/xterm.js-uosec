const arg = require('arg');
import helpMessage from 'raw-loader!../manpages/cat';

export default function cat (argv, env, filesystem) {
    try {
        const args = arg({'--help': Boolean, '-h': '--help'},  { argv: argv })
        if (args['--help']) {
            return helpMessage
        }
        // Determine the path to the directory to change to
        let path = ""

        if(args['_'].length != 1) {
            return "Error: No file specified. Use `cat --help` for help"

        } else {
            if (args['_'][0].startsWith("/")) {
                path = args['_'][0]
            } else {
                path = `${env["curr_directory"]}/${args['_'][0]}`.replace("//","/")
            }
        }

        if (path.endsWith("/")) {
            path = path.slice(0,-1)
        }

        console.log(path)

        // traverse the filesystem to check that the appropriate directory exists
        let list_path = path.split("/").slice(1)
        let filename = list_path.pop()

        if (list_path[0] == [""]) {
            list_path = [];
        } 
        console.log(list_path)
        let directory = filesystem
        for (var i = 0; i < (list_path.length); i++) {
            console.log(i)
            let found = false;
            // Check for the two special cases
            if (list_path[i] === ".."){
                if (directory.name === "/") {
                    found = true;
                } else {
                    directory = directory.parentDir
                    found = true;
                }
            }
            else if (list_path[i] === ".") {
                directory = directory;
                found = true;
            }
            else {
                for (var j = 0; j < directory.subdirectories.length; j++) {
                    console.log(directory.subdirectories, j)
                    if (directory.subdirectories[j].name === list_path[i]) {
                        found = true
                        directory = directory.subdirectories[j]
                        break;
                    }
                }
                for (var j = 0; j < directory.files.length; j++) {
                    if (directory.files[j].name === list_path[i]) {
                        return "Error, no such file or directory"
                    }
                }
            }
            if (!found) {
                return "Error: No such file or directory"
            }
        }

        // Find the file in directory and return its contents
        for (var i = 0; i < directory.files, i++;) {
            console.log(directory.files)
            if (directory.files[i].name == filename) {
               return `${directory.files[i].name}\r\n` 
            }
        }

        return "Error: No such file or directory"

    } catch (err) {
    if (err.code === 'ARG_UNKNOWN_OPTION') {
        return err.message
    } else {
        throw err;
    }}
}
