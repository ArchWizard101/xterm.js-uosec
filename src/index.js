import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';

// Our commands
import pwd from './commands/pwd.js'
import su from './commands/su.js'
import ls from './commands/ls.js'
import whoami from './commands/whoami.js'
import cd from './commands/cd.js'
import cat from './commands/cat.js'

const term = new Terminal({
    cursorBlink: "block"
});
const fitAddon = new FitAddon();
term.loadAddon(fitAddon);

let curr_line = "";
let entries = [""];
let entries_index = 0
let directories = require("./directories.json")
let env = {
    "curr_directory": "/",
    "curr_username": "u053c"
}

term.open(document.getElementById("terminal"));
fitAddon.fit();
term.prompt = () => {
    term.write(`[${env["curr_username"]}@challenger ${env["curr_directory"]}] $ `);
}
term.prompt()

// create an Observer instance
const resizeObserver = new ResizeObserver(entries => {
    fitAddon.fit();
    console.log("Resized term")
})

// start observing a DOM node
resizeObserver.observe(document.body)

// For directories
class Directory {
    name;
    parentDir;
    subdirectories;
    files;

    constructor(name, parentDir) {
        this.name = name;
        this.parentDir = parentDir;
        this.subdirectories = [];
        this.files = [];
    }
}

// For files
class File {
    name;
    contents;
    owner;

    constructor(name, contents, owner) {
        this.name = name;
        this.contents = contents;
        this.owner = owner;
    }
}

// Retrieve objects accordingly
const fromJson = (jsonThing) => {
    var thisDir = new Directory(jsonThing.name, null);
    // Loop through contents
    for (var i = 0; i < jsonThing.contents.length; i++) {
        var entry = jsonThing.contents[i];
        console.log(entry.name);
        if (entry.type == 'directory') {
            // Subdirectory
            var subdir = fromJson(entry);
            subdir.parentDir = thisDir;
            thisDir.subdirectories.push(subdir);
        } else {
            // File
            var thisFile = new File(entry.name, entry.file_contents, entry.owner)
            thisDir.files.push(thisFile);
        }
    }
    return thisDir;
}

// Construct object
let filesystem = fromJson(directories);
filesystem.parentDir = filesystem;

const parse = (line) => {
    var argv = line.split(" ")
    if (argv[0] === 'su'){
        return su(argv.slice(1), env)
    }
    else if (argv[0] === 'pwd'){
        return pwd(argv.slice(1), env)
    }
    else if (argv[0] === 'ls'){
        return ls(argv.slice(1), env, filesystem)
    }
    else if (argv[0] === 'whoami'){
        return whoami(argv.slice(1), env, filesystem)
    }
    else if (argv[0] === 'cd'){
        return cd(argv.slice(1), env, filesystem)
    }
    else if (argv[0] === 'cat'){
        return cat(argv.slice(1), env, filesystem)
    }
    else {
        return `bash: ${argv[0]}: command not found...`
    }
}


term.onData(function(ev) {
    console.log(ev)
    if (ev === "\r") {
        entries_index = 0
        if (curr_line) {
            entries.push(curr_line);
            term.write("\r\n");
            var output = parse(curr_line)
            if (output) {
                term.write(`${output}\r\n`)
            }
            term.prompt();
            curr_line = ""

        }
    } else if (ev.charCodeAt(0) === 127) {
        // Backspace
        entries_index = 0
        if (curr_line != "") {
            curr_line = curr_line.slice(0, curr_line.length - 1);
            term.write("\b \b");
        }
    } else if (ev === "\x1b[A") {

        // clear current line
        for (var i = 0; i < curr_line.length; i++) {
            term.write("\b \b");
        }

        entries_index ++

        entries_index = Math.min(entries_index, entries.length)

        curr_line = entries[entries.length - entries_index] 

        term.write(curr_line)

    }
    else if (ev === "\x1b[B") {

        // clear current line
        for (var i = 0; i < curr_line.length; i++) {
            term.write("\b \b");
        }

        entries_index --

        entries_index = Math.max(entries_index, 0)

        curr_line = entries[entries.length - entries_index] 

        if (!curr_line) {
            curr_line = ""
        }

        term.write(curr_line)

    } else {
        entries_index = 0
        curr_line += ev;
        term.write(ev);
    }
});
